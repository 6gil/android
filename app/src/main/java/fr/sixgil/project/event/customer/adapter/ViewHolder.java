package fr.sixgil.project.event.customer.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import fr.sixgil.project.event.R;
import fr.sixgil.project.event.customer.EventActivity;
import fr.sixgil.project.event.customer.object.EventDetails;

class ViewHolder extends RecyclerView.ViewHolder{

    private CardView itemCard;
    private TextView tvTitleCardEvent, tvDateCardEvent, tvContentCardEvent;
    private ImageView imvCompagnyCardLogo;

    //itemView est la vue correspondante à 1 cellule
    ViewHolder(View itemView, final Context context) {
        super(itemView);

        //c'est ici que l'on fait nos findView
        itemCard = (CardView) itemView.findViewById(R.id.cv_priv_card);
        tvTitleCardEvent = (TextView) itemView.findViewById(R.id.tv_priv_title_card_event);
        tvDateCardEvent = (TextView) itemView.findViewById(R.id.tv_priv_date_card_event);
        tvContentCardEvent = (TextView) itemView.findViewById(R.id.tv_priv_content_card_event);
        imvCompagnyCardLogo = (ImageView) itemView.findViewById(R.id.iv_priv_logo_company_card_event);

        itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getTitleEvent = tvTitleCardEvent.getText().toString();

                Intent i = new Intent(context, EventActivity.class);
                i.putExtra("item_card_title_event", getTitleEvent);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }

    //puis ajouter une fonction pour remplir la cellule en fonction d'un MyObject
    void bind(EventDetails eventObj){
        tvTitleCardEvent.setText(eventObj.getTitle());
        tvDateCardEvent.setText(eventObj.getDate_event());
        tvContentCardEvent.setText(eventObj.getContent());
        Picasso.with(imvCompagnyCardLogo.getContext()).load(eventObj.getLogo()).centerCrop().fit().into(imvCompagnyCardLogo);
        System.out.println("image url : " + eventObj.getLogo());
    }
}