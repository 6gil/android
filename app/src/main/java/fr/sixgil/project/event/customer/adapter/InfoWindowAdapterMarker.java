package fr.sixgil.project.event.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import fr.sixgil.project.event.R;

public class InfoWindowAdapterMarker implements GoogleMap.InfoWindowAdapter{

    private Context context;
    LayoutInflater layoutInflater;

    public InfoWindowAdapterMarker(LayoutInflater inflater){

        layoutInflater = inflater;
    }

    @Override
    public View getInfoContents(Marker marker) {

        View infoView = layoutInflater.inflate(R.layout.customer_marker_windows, null);

        TextView tvTitle = (TextView) infoView.findViewById(R.id.tv_customer_title_marker);
        tvTitle.setText(marker.getTitle());
        TextView tvContent = (TextView) infoView.findViewById(R.id.tv_customer_content_marker);
        tvContent.setText(marker.getSnippet());
        ImageView ivLogo = (ImageView) infoView.findViewById(R.id.imv_customer_marker);


        return infoView;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }
}
