package fr.sixgil.project.event.customer.object;


    public class EventDetails {
        private String title;
        private String logo;
        private String date_event;
        private String content;
        private Double latitute;
        private Double longitude;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getDate_event() {
            return date_event;
        }

        public void setDate_event(String date_event) {
            this.date_event = date_event;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Double getLatitute() {
            return latitute;
        }

        public void setLatitute(Double latitute) {
            this.latitute = latitute;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public EventDetails(String title, String logo, String date_event, String content, Double latitude, Double longitude) {

            this.title = title;
            this.logo = logo;
            this.date_event = date_event;
            this.content = content;
            this.latitute = latitude;
            this.longitude = longitude;

        }


    }

