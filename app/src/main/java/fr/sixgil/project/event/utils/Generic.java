package fr.sixgil.project.event.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.List;
import java.util.Random;

public class Generic {


    public static Integer getRandomIntItemFromList(List<Integer> listInt){

        try {
            return listInt.get((new Random()).nextInt(listInt.size()));
        }
        catch (Throwable e){
            return null;
        }
    }

    public static boolean isNetworkReachable(Context context){

        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
