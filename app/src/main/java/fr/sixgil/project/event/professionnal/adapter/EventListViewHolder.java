package fr.sixgil.project.event.professionnal.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import fr.sixgil.project.event.R;
import fr.sixgil.project.event.professionnal.ProfessionalActivity;
import fr.sixgil.project.event.professionnal.object.Event;

public class EventListViewHolder extends RecyclerView.ViewHolder{

    private TextView title;
    private TextView date;
    private TextView commentaire;
    private ImageView logo;
    private FloatingActionButton eventEditFab;
    private FloatingActionButton commentsFab;

    Context context;

    // itemView est la vue correspondante à 1 cellule
    public EventListViewHolder(View itemView) {
        super(itemView);

        //c'est ici que l'on fait nos findView
        title = (TextView) itemView.findViewById(R.id.title_event_item);
        date = (TextView) itemView.findViewById(R.id.date_event_item);
        commentaire = (TextView) itemView.findViewById(R.id.text_event_item);
        logo = (ImageView) itemView.findViewById(R.id.logo_event_item);
        eventEditFab = (FloatingActionButton) itemView.findViewById(R.id.edit_event_fab);
        commentsFab = (FloatingActionButton) itemView.findViewById(R.id.comments_fab);

        eventEditFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(context, ProfessionalActivity.class);
                context.startActivity(myIntent);

            }
        });
    }

    // puis ajouter une fonction pour remplir la cellule en fonction d'un Event
    public void bind(Event myEvent){
        title.setText(myEvent.getTitre());
        commentaire.setText(myEvent.getCommentaire());
        date.setText(myEvent.getDate());
        Picasso.with(logo.getContext()).load(myEvent.getLogo()).into(logo);
    }

}