package fr.sixgil.project.event.professionnal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import fr.sixgil.project.event.R;

public class ProfessionalActivity extends AppCompatActivity {

    CardView cvManageEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professional_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        cvManageEvents = (CardView) findViewById(R.id.cv_pro_manage_events);
        setSupportActionBar(toolbar);


        cvManageEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ProfessionalActivity.this, EventsListActivity.class);
                ProfessionalActivity.this.startActivity(myIntent);
            }
        });


    }

}