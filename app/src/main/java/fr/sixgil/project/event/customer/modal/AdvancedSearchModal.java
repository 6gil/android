package fr.sixgil.project.event.customer.modal;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import fr.sixgil.project.event.R;

public class AdvancedSearchModal extends DialogFragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {

        View view = inflater.inflate(R.layout.modal_advanced_search, container);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

            return view;
    };
}
