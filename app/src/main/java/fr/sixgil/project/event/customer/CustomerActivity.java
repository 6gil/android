package fr.sixgil.project.event.customer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.drive.query.internal.MatchAllFilter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.sixgil.project.event.EventApplication;
import fr.sixgil.project.event.LoginActivity;
import fr.sixgil.project.event.R;
import fr.sixgil.project.event.customer.adapter.InfoWindowAdapterMarker;
import fr.sixgil.project.event.customer.modal.AdvancedSearchModal;
import fr.sixgil.project.event.customer.modal.DetailsEventModal;
import fr.sixgil.project.event.customer.object.EventDetails;
import fr.sixgil.project.event.models.Customer;
import fr.sixgil.project.event.utils.Generic;

import static fr.sixgil.project.event.R.id.map;
import fr.sixgil.project.event.helper.SQLiteHandler;
import fr.sixgil.project.event.helper.SessionManager;
import io.socket.client.Socket;

public class CustomerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback{
    private Button btnLogout;
    MapFragment mapFragment;
    private GoogleMap Gmap;
    Context context;
    final int RQS_GooglePlayServices = 1;
    private SessionManager session;
    private SQLiteHandler db;
    private Customer customer;
    private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnLogout = (Button) findViewById(R.id.action_logout);
        setContentView(R.layout.activity_main_customer);
        context = this;
        mSocket = EventApplication.socket();
        if (getIntent().hasExtra("customer")) {
            customer = (Customer) getIntent().getExtras().get("customer");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabSearch = (FloatingActionButton) findViewById(R.id.fab_search);
        fabSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                showAdvancedSearchModal();
            }
        });


        FloatingActionButton fabList = (FloatingActionButton) findViewById(R.id.fab_list);
        fabList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ListEventsActivity.class);
                startActivity(i);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Map Configuration
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        /*if (!session.isLoggedIn()) {
            logoutUser();
        }*/
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {

            //Intent i = new Intent(getApplicationContext(), LoginActivity.class);
           // startActivity(i);
            //btnLogout.setOnClickListener(new View.OnClickListener() {

              // @Override
               // public void onClick(View v) {
                   logoutUser();
                //}
           // });
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(CustomerActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Gmap = googleMap;

        Gmap.setInfoWindowAdapter(new InfoWindowAdapterMarker(getLayoutInflater()));
        List<EventDetails> arrPreviewInfos = addItemEvent();


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Gmap.setMyLocationEnabled(true);
        Gmap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);


/*        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);*/

        LatLng lyon = new LatLng(45.75, 4.85);
        System.out.println("arr size : " + arrPreviewInfos.size());

        List<Integer> listResMarkerIcon = new ArrayList<>();
        listResMarkerIcon.add(R.mipmap.ic_marker_bar);
        listResMarkerIcon.add(R.mipmap.ic_marker_clothes);
        listResMarkerIcon.add(R.mipmap.ic_marker_hightech);
        listResMarkerIcon.add(R.mipmap.ic_marker_event);
        listResMarkerIcon.add(R.mipmap.ic_marker_restaurant);

        for(int i= 0; i < arrPreviewInfos.size(); i++){
            LatLng rlocal = new LatLng(arrPreviewInfos.get(i).getLatitute(), arrPreviewInfos.get(i).getLongitude());
            Gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(lyon, 10));
            Gmap.addMarker(new MarkerOptions()
                    .title(arrPreviewInfos.get(i).getTitle())
                    .snippet(arrPreviewInfos.get(i).getContent())
                    .position(rlocal)
                    .icon(BitmapDescriptorFactory.fromResource(Generic.getRandomIntItemFromList(listResMarkerIcon)))).showInfoWindow();
        }

        Gmap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {
                showDetailsEventModal();
            }
        });

    }

    @Override
    protected void onResume() {

        super.onResume();

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if (resultCode == ConnectionResult.SUCCESS){
            Toast.makeText(getApplicationContext(),
                    "Google Play Service activés",
                    Toast.LENGTH_LONG).show();
        }else{
            GooglePlayServicesUtil.getErrorDialog(resultCode, this, RQS_GooglePlayServices);
        }
    }

    private void showAdvancedSearchModal() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        AdvancedSearchModal advancedSearchModal = new AdvancedSearchModal();
        advancedSearchModal.setCancelable(true);
        advancedSearchModal.show(fragmentManager, "");
    }

    private void showDetailsEventModal() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        DetailsEventModal detailsEventModal = new DetailsEventModal();
        detailsEventModal.setCancelable(true);
        detailsEventModal.show(fragmentManager, "");
    }

    private List<EventDetails> addItemEvent(){

        List<EventDetails> listEvents = new ArrayList<>();

        for(int i=1; i < 9; i++){
            Double randomlat = Math.random() * (46 - 45.7) + 45.7;
            Double randomlong = Math.random() * (5 - 4.7) + 4.7;
            listEvents.add(new EventDetails("Title de l'évènement " + i, "https://unsplash.it/200/300/?random", "date1 to date2", "Ici contenu de l'évènement " + i, randomlat, randomlong));
        }

/*        listEvents.add(new EventDetails("Title event 2", "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), 45.75 + randomNumber, 4.85 + randomNumber));
        listEvents.add(new EventDetails("Title event 3", "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), 45.75 + randomNumber, 4.85 + randomNumber));
        listEvents.add(new EventDetails("Title event 4", "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), 45.75 + randomNumber, 4.85 + randomNumber));
        listEvents.add(new EventDetails("Title event 5", "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), 45.75 + randomNumber, 4.85 + randomNumber));
        listEvents.add(new EventDetails("Title event 6", "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), 45.75 + randomNumber, 4.85 + randomNumber));
        listEvents.add(new EventDetails("Title event 7", "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), 45.75 + randomNumber, 4.85 + randomNumber));
        listEvents.add(new EventDetails("Title event 8", "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), 45.75 + randomNumber, 4.85 + randomNumber));
        listEvents.add(new EventDetails("Title event 9", "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), 45.75 + randomNumber, 4.85 + randomNumber));*/

        return listEvents;
    }
}
