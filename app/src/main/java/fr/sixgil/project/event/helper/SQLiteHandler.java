package fr.sixgil.project.event.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by P2 on 09/04/2017.
 */

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "photondeomod2";

    // Login table name
    private static final String TABLE_LOGIN = "priv";
    private static final String TABLE_PRO = "pro";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_UID = "uid";
    private static final String KEY_CREATED_AT = "created_at";

    private static final String KEY_ID_PRO = "id";
    private static final String KEY_NAME_PRO = "name";
    private static final String KEY_EMAIL_PRO = "email";
    private static final String KEY_UID_PRO = "uid";
    private static final String KEY_CREATED_AT_PRO = "created_at";
    private static final String KEY_RAISON_SOCIAL_PRO = "raison_social";
    private static final String KEY_CODE_SIRET_PRO = "code_siret";
    private static final String KEY_CP_PRO = "cp";
    private static final String KEY_DOMAINE_PRO = "domaine";
    private static final String KEY_ADRESSE_PRO = "adresse";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_LOGIN + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE," + KEY_UID + " INT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        String CREATE_PRO_TABLE = "CREATE TABLE " + TABLE_PRO + "("
                + KEY_ID_PRO + " INTEGER PRIMARY KEY," + KEY_NAME_PRO + " TEXT,"
                + KEY_EMAIL_PRO + " TEXT UNIQUE," + KEY_UID_PRO + "INT,"+ KEY_CREATED_AT_PRO +"TEXT,"
                + KEY_RAISON_SOCIAL_PRO+ " TEXT," +KEY_CODE_SIRET_PRO + "TEXT," +KEY_CP_PRO + " TEXT" +KEY_DOMAINE_PRO + " TEXT" + ")";
        db.execSQL(CREATE_PRO_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Creating Tables
    //@Override
  //  public void onCreate2(SQLiteDatabase db) {


       // Log.d(TAG, "Database tables created");
   // }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN);

        // Create tables again
        onCreate(db);
    }

    // Upgrading database
    //@Override
    public void onUpgrade2(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRO);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addUser(String name, String email, String uid, String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name); // Name
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_UID, uid); // Email
        values.put(KEY_CREATED_AT, created_at); // Created At

        // Inserting Row
        long id = db.insert(TABLE_LOGIN, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    public void addPro(String name, String email, String uid, String created_at, String domaine, String siret, String adresse, String cp) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME_PRO, name); // Name
        values.put(KEY_EMAIL_PRO, email); // Email
        values.put(KEY_UID_PRO, uid); // Uid
        values.put(KEY_CREATED_AT, created_at); // Created At
        values.put(KEY_DOMAINE_PRO, domaine); // domaine
        values.put(KEY_CODE_SIRET_PRO, siret); // siret
        values.put(KEY_ADRESSE_PRO, adresse); // adresse
        values.put(KEY_CP_PRO, cp); // siret

        // Inserting Row
        long id = db.insert(TABLE_PRO, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New pro inserted into sqlite: " + id);
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_LOGIN;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("name1", cursor.getString(1));
            user.put("email", cursor.getString(2));
            user.put("uid", cursor.getString(3));
            user.put("created_at", cursor.getString(4));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getProDetails() {
        HashMap<String, String> pro = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_PRO ;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            pro.put("name", cursor.getString(1));
            pro.put("email", cursor.getString(2));
            pro.put("uid", cursor.getString(3));
            pro.put("created_at", cursor.getString(4));
            pro.put("domaine", cursor.getString(5));
            pro.put("code_siret", cursor.getString(6));
            pro.put("adresse", cursor.getString(7));
            pro.put("cp", cursor.getString(8));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + pro.toString());

        return pro;
    }

    /**
     * Getting user login status return true if rows are there in table
     * */
    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + TABLE_LOGIN;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();

        // return row count
        return rowCount;
    }

    public int getRowCount2() {
        String countQuery = "SELECT  * FROM " + TABLE_PRO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();

        // return row count
        return rowCount;
    }


    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_LOGIN, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }

}
