package fr.sixgil.project.event.professionnal;

    import android.os.Bundle;
    import android.support.v7.app.AppCompatActivity;
    import android.support.v7.widget.LinearLayoutManager;
    import android.support.v7.widget.RecyclerView;

    import java.util.ArrayList;
    import java.util.List;

    import fr.sixgil.project.event.R;
    import fr.sixgil.project.event.professionnal.adapter.EventListAdapter;
    import fr.sixgil.project.event.professionnal.object.Event;

public class EventsListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private List<Event> events = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professional_list);

        //remplir la ville
        ajouter_events();

        recyclerView = (RecyclerView) findViewById(R.id.events_listView);

        //définit l'agencement des cellules, ici de façon verticale, comme une ListView
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //puis créer un MyAdapter, lui fournir notre liste de villes.
        //cet adapter servira à remplir notre recyclerview
        recyclerView.setAdapter(new EventListAdapter(events));
    }

    private void ajouter_events() {
        events.add(new Event( 1, "Soirée Mousse", getResources().getString(R.string.lorem_ipsum), "29/04/2017", R.mipmap.example_logo));
        events.add(new Event( 2, "Promo Exceptionel !", getResources().getString(R.string.lorem_ipsum), "01/04/2017", R.mipmap.example_logo));
        events.add(new Event( 3, "Liquidation de stock", getResources().getString(R.string.lorem_ipsum), "12/04/2017", R.mipmap.example_logo));
        events.add(new Event( 4, "Soldes", getResources().getString(R.string.lorem_ipsum), "12/04/2017", R.mipmap.example_logo));
        events.add(new Event( 5, "Tombola", getResources().getString(R.string.lorem_ipsum), "12/04/2017", R.mipmap.example_logo));
        events.add(new Event( 6, "Meetup", getResources().getString(R.string.lorem_ipsum), "21/04/2017", R.mipmap.example_logo));
    }
}

