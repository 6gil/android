package fr.sixgil.project.event.utils;


import android.os.AsyncTask;
import android.os.Bundle;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpGet extends AsyncTask<String,Void,String> {

    @Override
    protected String doInBackground(String... siren) {

        StringBuilder result = new StringBuilder();
        HttpURLConnection urlConnection = null;
        String strSiren = siren[0];
        URL url = null;
        InputStream in = null;
        String line;

        try {

            if (!strSiren.isEmpty()) {
                url = new URL("https://firmapi.com/api/v1/companies/" + strSiren);
            }

            if (url != null) {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                if(urlConnection.getResponseCode() != 404){
                    in = urlConnection.getInputStream();
                }else{
                    in = urlConnection.getErrorStream();
                }

            }

            assert in != null;
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

        }catch( Exception e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return result.toString();
    }

    @Override
    protected void onPostExecute(String result){

        //    System.out.println("JSON String from GET METHOD : " + result);
    }
}
