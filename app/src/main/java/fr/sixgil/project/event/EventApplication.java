package fr.sixgil.project.event;

import io.socket.client.IO;
import io.socket.client.Socket;

import java.net.URISyntaxException;

public class EventApplication {

    private static Socket mSocket;

    public static Socket socket ()
    {
        if (mSocket != null) {
            if (!mSocket.connected()) {
                mSocket.connect();
            }
            return mSocket;
        }
        try {
            IO.Options opts = new IO.Options();
            opts.reconnection = true;
            opts.reconnectionDelay = 500;
            opts.timeout = -1;
            mSocket = IO.socket(Constantes.CHAT_SERVER_URL, opts);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        mSocket.connect();
        return mSocket;
    }
}