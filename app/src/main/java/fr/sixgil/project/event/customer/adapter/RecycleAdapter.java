package fr.sixgil.project.event.customer.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.sixgil.project.event.R;
import fr.sixgil.project.event.customer.object.EventDetails;

public class RecycleAdapter extends RecyclerView.Adapter<ViewHolder> {

    List<EventDetails> list;
    Context context;

    //ajouter un constructeur prenant en entrée une liste
    public RecycleAdapter(List<EventDetails> list, Context context) {
        this.list = list;
        this.context = context;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.customer_item_card_event,viewGroup,false);
        return new ViewHolder(view, context);
    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    @Override
    public void onBindViewHolder(ViewHolder myViewHolder, int position) {
        EventDetails myObject = list.get(position);
        myViewHolder.bind(myObject);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
