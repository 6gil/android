package fr.sixgil.project.event.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ugo on 13/06/2017.
 */

public class Customer implements Serializable {
    private String id;
    private String pseudo;
    private String email;
    private String lastname;
    private String firstname;
    private String fullname;
    private String password;

    public Customer ()
    {
        this.password = null;
    }

    /**
     * @param _id
     * @param pseudo
     * @param email
     * @param lastname
     * @param firstname
     * @param fullname
     */
    public Customer (String _id, String pseudo, String email, String lastname, String firstname, String fullname)
    {
        id = _id;
        this.pseudo = pseudo;
        this.email = email;
        this.lastname = lastname;
        this.firstname = firstname;
        this.fullname = fullname;
    }

    public static Customer fromJSONObject(JSONObject object) throws JSONException {
        try {
            String id = object.getString("_id");
            String pseudo = object.getString("pseudo");
            String email = object.getString("email");
            String lastname = object.getString("lastname");
            String firstname = object.getString("firstname");
            String fullname = object.getString("fullname");
            return new Customer(id, pseudo, email, lastname, firstname, fullname);
        } catch (JSONException e) {
            e.getStackTrace();
        }

        return null;
    }

    public Customer setPassword(String password)
    {
        this.password = password;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public String getId() {
        return id;
    }

    public Customer setId(String id) {
        this.id = id;
        return this;
    }

    public String getPseudo() {
        return pseudo;
    }

    public Customer setName(String pseudo) {
        this.pseudo = pseudo;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Customer setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public Customer setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public Customer setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getFullname() {
        return fullname;
    }

    public Customer setFullname(String fullname) {
        this.fullname = fullname;
        return this;
    }
}
