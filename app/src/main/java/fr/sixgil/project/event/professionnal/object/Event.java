package fr.sixgil.project.event.professionnal.object;

import android.graphics.drawable.Drawable;

import java.sql.Time;

/**
 * Created by Olivier on 20/03/2017.
 */

public class Event {

    private Integer id;
    private String titre;
    private String commentaire;
    private String date;
    private Integer logo;

    public Event( Integer id, String titre, String commentaire, String debut, Integer logo ) {
        this.id             = id;
        this.titre          = titre;
        this.commentaire    = commentaire;
        this.date           = debut;
        this.logo           = logo;
    }

    //getters & setters
    public String getTitre() { return titre; }
    public void setTitre(String titre) { this.titre = titre; }

    public String getCommentaire() { return commentaire; }
    public void setCommentaire(String commentaire) { this.commentaire = commentaire; }

    public String getDate() { return date; }
    public void setDate(String date) { this.date = date;}


    public int getLogo() { return logo; }
    public void setLogo(Integer logo) { this.logo = logo; }
}