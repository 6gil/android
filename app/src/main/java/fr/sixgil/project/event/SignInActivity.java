package fr.sixgil.project.event;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import fr.sixgil.project.event.models.Customer;
import fr.sixgil.project.event.utils.Generic;
import fr.sixgil.project.event.utils.HttpGet;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SignInActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        View rootView;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            switch (getArguments().getInt(ARG_SECTION_NUMBER))
            {
                case 1: {

                    rootView = inflater.inflate(R.layout.private_sign_in, container, false);

                    final Context context = rootView.getContext();

                    Button btnSignInPriv = (Button) rootView.findViewById(R.id.btn_priv_sign_in);

                    btnSignInPriv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Socket socket = EventApplication.socket();

                            EditText edtUserName = (EditText) rootView.findViewById(R.id.edt_sign_in_priv_name);
                            EditText edtUserPassword = (EditText) rootView.findViewById(R.id.edt_sign_in_priv_password);
                            EditText edtUserEmail = (EditText) rootView.findViewById(R.id.edt_sign_in_priv_email);
                            EditText edtFirstname = (EditText) rootView.findViewById(R.id.edt_sign_in_priv_surname);
                            EditText edtPseudo = (EditText) rootView.findViewById(R.id.edt_sign_in_priv_pseudo);

                            String getFirstname = edtFirstname.getText().toString().trim();
                            String getLastname = edtUserName.getText().toString().trim();
                            String getPassword = edtUserPassword.getText().toString().trim();
                            String getEmail = edtUserEmail.getText().toString().trim();
                            String getPseudo = edtPseudo.getText().toString().trim();

                            if (TextUtils.isEmpty(getFirstname)) {
                                edtFirstname.setError(getString(R.string.error_field_required));
                                edtFirstname.requestFocus();
                                return;
                            }
                            if (TextUtils.isEmpty(getLastname)) {
                                edtUserName.setError(getString(R.string.error_field_required));
                                edtUserName.requestFocus();
                                return;
                            }
                            if (TextUtils.isEmpty(getEmail)) {
                                edtUserEmail.setError(getString(R.string.error_field_required));
                                edtUserEmail.requestFocus();
                                return;
                            }
                            if (TextUtils.isEmpty(getPassword)) {
                                edtUserPassword.setError(getString(R.string.error_field_required));
                                edtUserPassword.requestFocus();
                                return;
                            }
                            if (TextUtils.isEmpty(getPseudo)) {
                                edtPseudo.setError(getString(R.string.error_field_required));
                                edtPseudo.requestFocus();
                                return;
                            }

                            if(Generic.isNetworkReachable(context)){

                                final JSONObject customer = new JSONObject();
                                try {
                                    customer.put("pseudo", getPseudo);
                                    customer.put("firstname", getFirstname);
                                    customer.put("lastname", getLastname);
                                    customer.put("password", getPassword);
                                    customer.put("email", getEmail);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                socket.on("customer create failed", new Emitter.Listener() {
                                    @Override
                                    public void call(Object... args) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(), R.string.error_connexion_socket, Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                });

                                socket.on("customer created", new Emitter.Listener() {
                                    @Override
                                    public void call(Object... args) {

                                        JSONObject object = (JSONObject) args[0];
                                        try {
                                            final Customer customer = Customer.fromJSONObject(object);
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getActivity().getApplicationContext(), "Votre compte a été crée avec succès", Toast.LENGTH_LONG).show();
                                                    Intent i = new Intent(getActivity(), LoginActivity.class);
                                                    i.putExtra("login", customer != null ? customer.getPseudo() : null);
                                                    startActivity(i);
                                                }
                                            });
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                                socket.emit("create customer", customer);

                            }else{
                                Toast.makeText(context, R.string.error_connexion_device, Toast.LENGTH_LONG).show();
                            }

                            System.out.println("sign in button (priv) : " + getPseudo);
                        }
                    });

                    break;
                }
                case 2: {
                    rootView = inflater.inflate(R.layout.professional_sign_in, container, false);

                    Button btnSignInPro = (Button) rootView.findViewById(R.id.btn_pro_sign_in);


                    btnSignInPro.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            EditText edtSiret = (EditText) rootView.findViewById(R.id.edt_sign_in_pro_siren);
                            EditText edtPass = (EditText) rootView.findViewById(R.id.edt_sign_in_pro_password);
                            EditText edtConfPass = (EditText) rootView.findViewById(R.id.edt_sign_in_pro_conf_password);

                            String getSiret = edtSiret.getText().toString();
                            String getPass = edtPass.getText().toString();
                            String getConfPass = edtConfPass.getText().toString();


                            if (TextUtils.isEmpty(getSiret)) {
                                edtSiret.setError(getString(R.string.error_field_required));
                                edtSiret.requestFocus();
                                return;
                            }

                            if (TextUtils.isEmpty(getPass)) {
                                edtPass.setError(getString(R.string.error_field_required));
                                edtPass.requestFocus();
                                return;
                            }
                            if (TextUtils.isEmpty(getConfPass)) {
                                edtConfPass.setError(getString(R.string.error_field_required));
                                edtConfPass.requestFocus();
                                return;
                            }
                            if(!getPass.equals(getConfPass)){

                                edtPass.setError(getString(R.string.error_field_conf_pass));
                                edtConfPass.setError(getString(R.string.error_field_conf_pass));
                                return;
                            }

                            try {

                                if(Generic.isNetworkReachable(getContext())){

                                    String response = new HttpGet().execute(getSiret).get();
                                    JSONObject jb = new JSONObject(response);
                                    String status = jb.getString("status");
                                    if(status.equals("success")){

                                        JSONObject company = jb.getJSONObject("company");
                                        String getSiren = company.getString("siren");
                                        JSONObject names = company.getJSONObject("names");
                                        String getDenomination = names.getString("denomination");
                                        String getLegalForm = company.getString("legal_form");
                                        String getAddress = company.getString("address");
                                        String getPostalCode = company.getString("postal_code");
                                        String getCity = company.getString("city");
                                        String getVATNumber = company.getString("vat_number");
                                        String getCapital = company.getString("capital");
                                        String getActivity = company.getString("activity");
                                        Bundle bundle = new Bundle();
                                        bundle.putString("siren", getSiren);
                                        bundle.putString("denomination", getDenomination);
                                        bundle.putString("legal_form", getLegalForm);
                                        bundle.putString("address", getAddress);
                                        bundle.putString("postal_code", getPostalCode);
                                        bundle.putString("city", getCity);
                                        bundle.putString("vat_number", getVATNumber);
                                        bundle.putString("capital", getCapital);
                                        bundle.putString("activity", getActivity);
                                        bundle.putString("password", getPass);

                                        showCompanyInfos(getActivity(),bundle);

                                    }else{

                                        Toast.makeText(getActivity(), "Oops... Il semblerait que le code SIREN n'est pas été reconnu dans notre base", Toast.LENGTH_LONG).show();
                                    }

                                }else{

                                    Toast.makeText(getContext(), R.string.error_connexion_device, Toast.LENGTH_LONG).show();
                                 }


                            } catch (InterruptedException | ExecutionException | JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    break;
                }

            }
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Je suis un particulier";
                case 1:
                    return "Je suis un professionnel";
            }
            return null;
        }
    }

    private static void showCompanyInfos(FragmentActivity activity, Bundle bundle){

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        SirenInfos sirenInfos = new SirenInfos();
        sirenInfos.setCancelable(true);
        sirenInfos.setArguments(bundle);
        sirenInfos.show(fragmentManager,"");
    }

}