package fr.sixgil.project.event;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import fr.sixgil.project.event.utils.Generic;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SirenInfos extends android.support.v4.app.DialogFragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {

        View view = inflater.inflate(R.layout.modal_siren_infos, container);
        Bundle bundle = getArguments();
        final TextView tvSiren = (TextView) view.findViewById(R.id.modal_num_siren);
        TextView tvDenomination = (TextView) view.findViewById(R.id.modal_denomination);
        TextView tvAddress = (TextView) view.findViewById(R.id.modal_company_adress);
        TextView tvCity = (TextView) view.findViewById(R.id.modal_company_city);
        TextView tvLF = (TextView) view.findViewById(R.id.modal_legal_form);
        TextView tvNTVA = (TextView) view.findViewById(R.id.modal_number_tva);
        TextView tvPostalCode = (TextView) view.findViewById(R.id.modal_postal_code);
        TextView tvCapital = (TextView) view.findViewById(R.id.modal_capital);
        Button btnValidate = (Button) view.findViewById(R.id.btn_valide_infos);

        final String siren = bundle.getString("siren");
        final String denomination = bundle.getString("denomination");
        final String address = bundle.getString("address");
        final String city = bundle.getString("city");
        final String legalForm = bundle.getString("legal_form");
        final String vatNumber = bundle.getString("vat_number");
        final String postalCode = bundle.getString("postal_code");
        final String password = bundle.getString("password");
        final String capital = bundle.getString("capital");

        tvSiren.setText(bundle.getString("siren"));
        tvDenomination.setText(bundle.getString("denomination"));
        tvAddress.setText(bundle.getString("address"));
        tvCity.setText(bundle.getString("city"));
        tvLF.setText(bundle.getString("legal_form"));
        tvNTVA.setText(bundle.getString("vat_number"));
        tvPostalCode.setText(bundle.getString("postal_code"));
        tvCapital.setText(String.format("%,.2f", Double.parseDouble(capital)) + " €");

        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("Network state : " + Generic.isNetworkReachable(getContext()));

                    Socket socket = EventApplication.socket();

                    final JSONObject professional = new JSONObject();
                    try {
                        professional.put("siren", siren);
                        professional.put("denomination", denomination);
                        professional.put("address", address);
                        professional.put("city", city);
                        professional.put("legal_form", legalForm);
                        professional.put("vat_number", vatNumber);
                        professional.put("postal_code", postalCode);
                        professional.put("password", password);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    socket.on("professional create failed", new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            System.out.println("Failed to create professional account : " + args[0]);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), "Oops.. une erreur est survenue, réessayer ultérieurement", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });

                    socket.on("professional created", new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            System.out.println("Successfully created professional account : " + args[0]);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), "Votre compte a été crée avec succès", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(getContext(), LoginActivity.class);
                                    i.putExtra("login", siren);
                                    startActivity(i);
                                }
                            });
                        }
                    });

                    socket.emit("create professional", professional);

            }
        });

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        return view;
    };

}
