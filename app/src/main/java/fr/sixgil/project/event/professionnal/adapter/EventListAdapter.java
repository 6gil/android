package fr.sixgil.project.event.professionnal.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.sixgil.project.event.R;
import fr.sixgil.project.event.professionnal.EventsListActivity;
import fr.sixgil.project.event.professionnal.object.Event;

/**
 * Created by Olivier on 20/03/2017.
 */
public class EventListAdapter extends RecyclerView.Adapter<EventListViewHolder> {

    List<Event> EventList;
    private final View.OnClickListener itemListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           /* Intent myIntent = new Intent(EventsListActivity.this, ProfessionalEventActivity.class);
            EventsListActivity.this.startActivity(myIntent);*/
        }
    };

    //ajouter un constructeur prenant en entrée une liste
    public EventListAdapter(List<Event> list) {
        this.EventList = list;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    @Override
    public EventListViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_professional_event,viewGroup,false);
        return new EventListViewHolder(view);
    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    @Override
    public void onBindViewHolder(EventListViewHolder myViewHolder, int position) {
        Event myEvent = EventList.get(position);
        myViewHolder.bind(myEvent);
    }

    @Override
    public int getItemCount() {
        return EventList.size();
    }

}
