package fr.sixgil.project.event.customer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import fr.sixgil.project.event.R;
import fr.sixgil.project.event.customer.adapter.RecycleAdapter;
import fr.sixgil.project.event.customer.object.EventDetails;
import fr.sixgil.project.event.utils.Generic;

public class ListEventsActivity extends AppCompatActivity {

    private RecyclerView rvList;
    private List<EventDetails> listEvents = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       System.out.println("Is network connected : " + Generic.isNetworkReachable(this));

       addItemEvent();

        rvList = (RecyclerView) findViewById(R.id.rv_customer_list);
        //rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setAdapter(new RecycleAdapter(listEvents, getApplicationContext()));

    }

    private void addItemEvent(){

        for(int i=1; i < 9; i++){
            Double randomlat = Math.random() * (46 - 45.7) + 45.7;
            Double randomlong = Math.random() * (5 - 4.7) + 4.7;
            listEvents.add(new EventDetails("Title de l'évènement " + i, "https://unsplash.it/200/300/?random", "date1 to date2", getResources().getString(R.string.lorem_ipsum), randomlat, randomlong));
        }

    }

}
