package fr.sixgil.project.event;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import fr.sixgil.project.event.customer.CustomerActivity;
import fr.sixgil.project.event.models.Customer;
import fr.sixgil.project.event.professionnal.ProfessionalActivity;
import fr.sixgil.project.event.utils.Generic;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static java.security.AccessController.getContext;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    Button lkSignIn;
    EditText edtUserName;
    EditText edtUserPassword;
    String getUserName;
    String getGetUserPassword;
    Socket mSocket;
    String idPro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mSocket = EventApplication.socket();

        btnLogin = (Button) findViewById(R.id.btn_login);
        lkSignIn = (Button) findViewById(R.id.lk_sign_in);
        edtUserName = (EditText) findViewById(R.id.edt_login_username);
        edtUserPassword = (EditText) findViewById(R.id.edt_login_password);

        Intent getLoginFromSignIn = getIntent();
        if(getLoginFromSignIn.hasExtra("login")){

            edtUserName.setText(getLoginFromSignIn.getExtras().getString("login"));
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getUserName = edtUserName.getText().toString();
                getGetUserPassword = edtUserPassword.getText().toString();

                if(Generic.isNetworkReachable(getApplicationContext())){

                    mSocket.on("customer logged",onLoginCustomer);
                    mSocket.on("professional logged",onLoginPro);
                    mSocket.on("failed to login", new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), R.string.error_connexion_socket, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });

                    JSONObject user = new JSONObject();
                    try {
                        user.put("login", getUserName);
                        user.put("password", getGetUserPassword);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    mSocket.emit("login", user);

                }else{
                    Toast.makeText(getApplicationContext(), R.string.error_connexion_device, Toast.LENGTH_LONG).show();
                }

            }
        });

        lkSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SignInActivity.class);
                startActivity(i);
            }
        });
    }

    public Emitter.Listener onLoginCustomer = new Emitter.Listener() {
                    @Override
                    public void call(final Object... args) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                try {
                                    JSONObject data = (JSONObject) args[0];
                                    Customer customer = Customer.fromJSONObject(data);
                                    Intent intent = new Intent(LoginActivity.this, CustomerActivity.class);
                                    intent.putExtra("customer", customer);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "Bienvenue !", Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

        }

    };
    public Emitter.Listener onLoginPro = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];

            String numPro;
            try {
                numPro = data.getString("_id");
                idPro = numPro;
            } catch (JSONException e) {
                return;
            }

            Intent intent = new Intent(getApplicationContext(), ProfessionalActivity.class);
            intent.putExtra("name", getUserName);
            intent.putExtra("password", getGetUserPassword);
            startActivity(intent);

            finish();

        }

    };

}
